FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/ms5-demo-0.0.1.da59fac.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/ms5-demo-0.0.1.da59fac.jar"]
