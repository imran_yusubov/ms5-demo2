package az.ingress.ms5demo.configurations;

import az.ingress.ms5demo.factory.HP;
import az.ingress.ms5demo.factory.IPrinter;
import az.ingress.ms5demo.factory.LG;
import az.ingress.ms5demo.services.IPayment;
import az.ingress.ms5demo.services.PaymentServiceImpl2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.atomic.AtomicInteger;

@Configuration
public class AppConfig {

    private AtomicInteger atomicInteger = new AtomicInteger(); //atomic

    @Bean
    //@Scope("prototype") //
    public IPayment paymentBean() {
        System.out.println("Created new payment service:" + atomicInteger.incrementAndGet());
        PaymentServiceImpl2 payment = new PaymentServiceImpl2();
        payment.setTaxAmount(18L);
        return payment;
    }


    @Bean(name = "HP")
    public IPrinter getPrinterHP() {
        return new HP();
    }

    @Bean(name = "LG")
    @Primary
    public IPrinter getPrinterLG() {
        return new LG();
    }
}
