package az.ingress.ms5demo.services;


public class PaymentServiceImpl implements IPayment {

    /*
        SINGLETON PATTERN
     */

    //any given time, we should have a single instance of the class
    private static PaymentServiceImpl instance;

    private PaymentServiceImpl() {
    }

    @Override
    public void pay(String account, Double amount) {
        System.out.println("PAYED :" + account + "  " + amount);
    }

    public static PaymentServiceImpl getInstance() {
        if (instance == null)
            synchronized (PaymentServiceImpl.class) {
                if (instance == null)
                    instance = new PaymentServiceImpl();
            }
        return
                instance;
    }
}
