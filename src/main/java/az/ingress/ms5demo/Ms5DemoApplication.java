package az.ingress.ms5demo;

import az.ingress.ms5demo.configurations.AppDetails;
import az.ingress.ms5demo.factory.IPrinter;
import az.ingress.ms5demo.factory.PrinterFactory;
import az.ingress.ms5demo.model.Student;
import az.ingress.ms5demo.services.IPayment;
import az.ingress.ms5demo.services.PaymentServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@AllArgsConstructor
public class Ms5DemoApplication implements CommandLineRunner {

    final IPayment iPayment = PaymentServiceImpl.getInstance(); //singleton :
    final IPayment iPayment2 = PaymentServiceImpl.getInstance();

    //@Autowired
    @Qualifier("HP")
    final IPrinter iPrinter;

    final IPayment iPayment3; //singleton :
    final IPayment iPayment4; //separate declaration and initialization

    final AppDetails appDetails;

    public static void main(String[] args) {
        SpringApplication.run(Ms5DemoApplication.class, args);
    }

    @Override
    public void run(String[] args) throws Exception {
        System.out.println("RUNNER :" + iPayment);
        System.out.println("RUNNER :" + iPayment2);

        System.out.println("RUNNER :" + iPayment3);
        System.out.println("RUNNER :" + iPayment4);


        IPrinter hp = PrinterFactory.getInstance("HP"); //factory, HP
        System.out.println(hp);
        IPrinter lg = PrinterFactory.getInstance("LG"); //factory, LG
        System.out.println(lg);


        //Bean
        //@Primary
        //Qualifier
        System.out.println(iPrinter);

//        Student student = Student.builder()  //Builder Pattern: used for initialization of classes with many optional fields
//                .age(1L)
//                .firstName("test")
//                .build();


        System.out.println(appDetails);
        appDetails.getGrades()
                .forEach((k, v) -> System.out.println("Key :" + k + " value: " + v));
    }
}
